const express = require("express");
const app = express();
const serverless = require('serverless-http');
const PORT = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// design file
app.use(express.static("public"));
app.set("view engine", "ejs");

// routers
app.get("/", (req, res) => {
  const hotelsData = [
    { id: 1, img: 'img/card-img.png', title: 'Radisson Blu', address: 'Tueager 5A, 8200 Aarhus, Denmark', price: '$450', gift: '3,605' },
    { id: 2, img: 'img/card-1.png', title: 'Wakeup Copenhagen', address: 'Tueager 5A, 8200 Aarhus, Denmark', price: '$2,500', gift: '1,500' },
    { id: 3, img: 'img/card-2.png', title: 'Scandic Hotels', address: 'Tueager 5A, 8200 Aarhus, Denmark', price: '$24,00', gift: '250' },
    { id: 4, img: 'img/Image.png', title: 'Cabinn Hotels', address: 'Tueager 5A, 8200 Aarhus, Denmark', price: '$560', gift: '470' },
    { id: 5, img: 'img/Image-1.png', title: 'Zleep Hotels', address: 'Tueager 5A, 8200 Aarhus, Denmark', price: '$695', gift: '340' },
    { id: 6, img: 'img/card-3.png', title: 'Comwell Hotels', address: 'Tueager 5A, 8200 Aarhus, Denmark', price: '$3,400', gift: '5,800' },
  ];
  res.render("index", { hotelsData });
});

app.get('/login', (req, res) => {
  res.render('login');
});

app.get('/register', (req, res) => {
  res.render('register');
});

app.get('/forgotpassword', (req, res) => {
  res.render('forgotpassword');
});

app.get('/checkmail', (req, res) => {
  res.render('checkmail');
});

app.get('/setpassword', (req, res) => {
  res.render('setpassword');
});

app.get('/resetpassword', (req, res) => {
  res.render('resetpassword');
});

app.get('/wakeup', (req, res) => {
  res.render('wakeup');
});

// server listening
app.listen(PORT, () => {
  console.log(`The app start on http://localhost:${PORT}`);
});

app.use('/.netlify/functions/index', app);
module.exports.handler = serverless(app);